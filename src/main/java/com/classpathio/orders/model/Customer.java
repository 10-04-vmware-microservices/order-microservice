package com.classpathio.orders.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="customers")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@EqualsAndHashCode(of = "email")
@ToString(exclude = "orders")
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String name;
	
	private String email;

	/*
	 * @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch =
	 * FetchType.LAZY, orphanRemoval = true)
	 * 
	 * @JsonManagedReference private Set<Order> orders;
	 */
	
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(
			name="customer_address",
			joinColumns = @JoinColumn(name="customer_id"),
			inverseJoinColumns = @JoinColumn(name="address_id")
			)
	@JsonManagedReference
	private Set<Address> addressSet;
	
	/*
	 * //scaffolding method public void addOrder(Order order) { if(this.orders ==
	 * null) { this.orders = new HashSet<>(); } this.orders.add(order);
	 * //order.setCustomer(this); }
	 */
	public void addAddress(Address address) {
		if(this.addressSet == null ) {
			this.addressSet = new HashSet<>();
		}
		this.addressSet.add(address);
		address.getCustomers().add(this);
	}

}
