package com.classpathio.orders.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="address")
@NoArgsConstructor
@AllArgsConstructor
@Builder

@ToString(exclude="customers")
@EqualsAndHashCode(exclude = "customers")
public class Address {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter
	private int id;
	@Setter @Getter
	private String addressLine1;
	@Setter @Getter
	private String city;
	@Setter @Getter
	private String state;
	@Setter @Getter
	private String zipCode;

	@ManyToMany(mappedBy = "addressSet")
	@JsonBackReference
	private Set<Customer> customers;
	

	public Set<Customer> getCustomers() {
		if(this.customers == null) {
			this.customers = new HashSet<>();
		}
		return customers;
	}


	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}
	
	


}
