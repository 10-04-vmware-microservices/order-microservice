package com.classpathio.orders.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.web.SecurityFilterChain;



@Configuration
public class AppSecurityConfig {

    /**
	 * Steps to perform the authorization
	 * 1. Validate the JWT token
	 *    - using the keys
	 * 2. From the JWT token extract the claims
	 *    - groups
	 * 3. Convert the groups to Spring security roles
	 * 4. Provides access/deny based on the roles
	 */   
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        //authorization rules to allow/deny
		http.cors().disable();
		http.csrf().disable();
        http.headers().frameOptions().disable();

        http.authorizeRequests().antMatchers("/login**", "/api/state/**", "/logout**", "/h2-console/**", "/actuator/**").permitAll()
            .antMatchers(HttpMethod.GET, "/api/orders**", "/api/orders/**")
					.hasAnyRole("Everyone", "super_admins", "admins")
            .antMatchers(HttpMethod.POST, "/api/orders**")
					.hasAnyRole("super_admins", "admins")
            .antMatchers(HttpMethod.DELETE, "/api/orders/**")
					.hasRole("super_admins")                   
			.and()
			.oauth2ResourceServer()
			  .jwt(); 
        
        return http.build();

    }

    @Bean
	public JwtAuthenticationConverter jwtAuthenticationConverter() {
		JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
		JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
		jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName("groups");
		jwtGrantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");
		jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
		return jwtAuthenticationConverter;
	}
}
