package com.classpathio.orders.exception;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Component
@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public Error handleInvalidOrderIdException(IllegalArgumentException exception) {
		System.out.println("exception caught :: "+ exception.getMessage());
		return new Error(100, exception.getMessage());
	}
	
	@ExceptionHandler(EntityNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public Error handleInvalidEntityException(EntityNotFoundException exception) {
		System.out.println("exception caught :: "+ exception.getMessage());
		return new Error(100, exception.getMessage());
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Set<String> handleInvalidData(MethodArgumentNotValidException exception) {
		System.out.println("exception caught :: "+ exception.getMessage());
		List<ObjectError> errors = exception.getAllErrors();
		Set<String> errorMessages = errors.stream().map(error -> error.getDefaultMessage()).collect(Collectors.toSet());
		return errorMessages;
	}
	
	

}

class Error {
	private int code;
	private String message;
	
	private Error() {}
	
	public Error(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}

